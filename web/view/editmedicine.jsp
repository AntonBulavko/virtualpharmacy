<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 17:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit medicine</title>
</head>
<body>
<header>
    <div>
        <p>Virtual pharmacy</p>
    </div>
    <ul>
        <li><a href="<%=request.getContextPath()%>/medicines">Medicines list</a></li>
    </ul>
    <ul>
        <li><a href="<%=request.getContextPath()%>/logout">Logout</a></li>
    </ul>
</header>
<h3>Edit medicine</h3>
<form method="post">
    <input type="hidden" value="${medicine.id}" name="id"/><br/>
    <label>Name
        <input name="name" value="${medicine.name}"/><br/>
    </label>
    <label>Price
        <input type="number" name="price" value="${medicine.price} "/><br/>
    </label>
    <label>Category
        <select name="category" value="${medicine.category}">
            <option value="tablet">tablet</option>
            <option value="vitamin">vitamin</option>
            <option value="syrup">syrup</option>
            <option value="spays">spray</option>
        </select><br/>
    </label>
    <label>Description
        <input name="description" value="${medicine.description}"/><br/>
    </label>
    <label>Prescription
        <select name="prescription" >
            <option value="true">Need</option>
            <option value="false">Don't need</option>
        </select>
    </label>
    <input type="submit" value="Send"/>
</form>
</body>
</html>
