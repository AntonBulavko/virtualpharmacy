<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.12.2019
  Time: 14:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cart</title>
</head>
<body>
<div class="alert alert-success center" role="alert">
    <c:out value="${Error}"/>
</div>
<table>
    <tr>
        <th>Medicine name</th>
        <th>Medicine price</th>
        <th>Medicine qty</th>
    </tr>
    <c:forEach var="cartItem" items="${cart.items}">
        <tr>
            <td><c:out value="${cartItem.medicine.name}"/></td>
            <td><c:out value="${cartItem.medicine.price}"/></td>
            <td>
                <form action="<c:url value='/updateCart'/>" method="post">
                    <input type="hidden" name="medicineId" value="${cartItem.medicine.id}">
                    <input type="number" name="qty" value="${cartItem.qty}" max="20">
                    <input type="submit" name="submit" value="recalculate price">
                </form>
            </td>
            <td>
                <a href='<c:url value="/deleteItem?id=${cartItem.medicine.id}"/>'>Delete</a>
            </td>
        </tr>
    </c:forEach>
    <td>
        Total price <c:out value="${cart.totalSum}"/>
    </td>
</table>
<c:if test="${sessionScope.containsValue(customer)}">
    <button onclick="location.href='/createOrder'">Create Order</button>
</c:if>
<c:if test="${!sessionScope.containsValue(customer)}">
    <button onclick="location.href='/login'">Login</button>
</c:if>
</body>
</html>
