<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Work
  Date: 08.01.2020
  Time: 23:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order items</title>
</head>
<body>
<table>
    <tr>
        <th>order id</th>
        <th>medicine name</th>
        <th> qty</th>
    </tr>
    <c:forEach var="orderItem" items="${orderItems}">
        <tr>
            <td><c:out value="${orderItem.order.orderId}"/></td>
            <td><c:out value="${orderItem.medicine.name}"/></td>
            <td><c:out value="${orderItem.qty}"/></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
