<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.12.2019
  Time: 18:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login page</title>
</head>
<body>
<h1>Login form</h1>
<div class="alert alert-success center" role="alert">
    <c:out value="${Error}"/>
</div>
<form method="post">
    <div>
        <label>Login</label><input type="text" name="login" required>
    </div>
    <div>
        <label>Password</label><input type="password" name="password" required>
    </div>
    <button type="submit">Login</button>
</form>

</body>
</html>
