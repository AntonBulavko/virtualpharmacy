<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://jakarta.apache.org/taglibs/standard/permittedTaglibs" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 11.12.2019
  Time: 13:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customer info page</title>
</head>
<body>
<table>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Login</th>
        <th>Password</th>
    </tr>
    <tr>
        <td><c:out value="${customer.name}"/></td>
        <td><c:out value="${customer.email}"/></td>
        <td><c:out value="${customer.address}"/></td>
        <td><c:out value="${customer.phone}"/></td>
        <td><c:out value="${customer.login}"/></td>
        <td><c:out value="${customer.password}"/></td>
    </tr>
    <td>
        <a href='<c:url value="/editCustomer?id=${customer.id}"/>'>Edit</a>
        <a href='<c:url value="/deleteCustomer"/>'>Delete</a>
    <td>
</table>
<table>
    <tr>
        <th>order id</th>
        <th>total sum</th>
    </tr>
    <c:forEach var="order" items="${orders}">
        <tr>
            <td><c:out value="${order.orderId}"/></td>
            <td><c:out value="${order.totalCost}"/></td>
            <td>
                <a href='<c:url value="/showOrderItems?id=${order.orderId}"/>'>Show ordered items</a>
            </td>
        </tr>
    </c:forEach>
</table>

<button onclick="location.href='/mainPage'">Main</button>
</body>
</html>
