<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Virtual Pharmacy</title>
</head>
<body>
<header>
    <div>
        <p>Virtual pharmacy</p>
    </div>
    <ul>
        <li><a href="<%=request.getContextPath()%>/medicines">Medicines list</a></li>
    </ul>

    <div class="alert alert-success center" role="alert">
        <c:out value="${Error}"/>
    </div>

</header>
<div>
    <div>
        <h2>Add medicine</h2>
    </div>
    <form method="post">
        <label>Name:
            <input type="text" name="name"><br/>
        </label>
        <label>Price:
            <input type="number" name="price"><br/>
        </label>
        <label>Category:
            <select name="category">
                <option value="tablet">tablet</option>
                <option value="vitamin">vitamin</option>
                <option value="syrup">syrup</option>
                <option value="spays">spray</option>

            </select>

        </label>
        <label>Description:
            <input type="text" name="description"><br/>
        </label>
        <label>Prescription:
            <select name="prescription">
                <option value="true">Need</option>
                <option value="false">Don't need</option>
            </select>
        </label>
        <button type="submit">Submit</button>
    </form>
</div>

</body>
</html>
