<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 19.12.2019
  Time: 11:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin page</title>
</head>
<body>
<div>
    <div>
        <button onclick="location.href='/registerManager'">Register new manager</button>
        <button onclick="location.href='/medicines'"size="30">Medicines catalog</button>
        <button onclick="location.href='/customers'">Customers list</button>
        <button onclick="location.href='/showManagers'">Managers list</button>
        <button onclick="location.href='/logout'">Logout</button>
    </div>
</div>
</body>
</html>
