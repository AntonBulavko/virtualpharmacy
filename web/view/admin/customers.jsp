<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 19.12.2019
  Time: 11:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customers</title>
</head>
<body>
<table>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Email</th>
        <th>Address</th>
        <th>Phone</th>
        <th>Login</th>

    </tr>
    <c:forEach var="customer" items="${customers}">
        <tr>
            <td><c:out value="${customer.id}"/></td>
            <td><c:out value="${customer.name}"/></td>
            <td><c:out value="${customer.email}"/></td>
            <td><c:out value="${customer.address}"/></td>
            <td><c:out value="${customer.phone}"/></td>
            <td><c:out value="${customer.login}"/></td>
            <td>
                <a href='<c:url value="/customerOrders?id=${customer.id}"/>'>Show customers orders</a>
            <td>
        </tr>
    </c:forEach>
</table>
<button onclick="location.href='/adminPage'">back</button>
</body>
</html>
