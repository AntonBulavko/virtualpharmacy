<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 19.12.2019
  Time: 13:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Managers</title>
</head>
<body>
<table>
    <tr>
        <th>Name</th>
        <th>Surname</th>
        <th>Login</th>
        <th>Password</th>
    </tr>
    <c:forEach var="manager" items="${managers}">
        <tr>
            <td><c:out value="${manager.firstName}"/></td>
            <td><c:out value="${manager.lastName}"/></td>
            <td><c:out value="${manager.login}"/></td>
            <td><c:out value="${manager.password}"/></td>
        </tr>
    </c:forEach>
</table>
<button onclick="location.href='/adminPage'">Back to menu</button>
</body>
</html>
