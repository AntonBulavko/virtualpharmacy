<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 19.12.2019
  Time: 12:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Customer orders</title>
</head>
<body>
<table>
    <tr>
        <th>order id</th>
        <th>total sum</th>
    </tr>
    <c:forEach var="order" items="${orders}">
        <tr>
            <td><c:out value="${order.orderId}"/></td>
            <td><c:out value="${order.totalCost}"/></td>
        </tr>
    </c:forEach>
</table>
<button onclick="location.href='/adminPage'">back</button>
</body>
</html>
