<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Hello page</title>
</head>
<body>
<div style="text-align: center">
    <h1>Main page</h1>
    <div>
        <div>
            <button onclick="location.href='/registerCustomer'" size="30">Register new customer</button>
            <button onclick="location.href='/login'"size="30">Login as customer</button>
            <button onclick="location.href='/loginAsManager'"size="30">Login as manager</button>
            <button onclick="location.href='/medicines'"size="30">Medicines catalog</button>
        </div>
    </div>
</div>
</body>
</html>
