<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 06.12.2019
  Time: 11:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Virtual pharmacy</title>
</head>
<body>
<div class="row">
    <div class="container">
        <h3 class="text-center">List of medicines</h3>
        <hr>
        <div class="container text-left">
            <c:if test="${sessionScope.containsValue(manager)}">
                <a href="<%=request.getContextPath()%>/add"
                   class="btn btn-success">Add medicine</a>
            </c:if>
        </div>
        <br>
        <table>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Category</th>
                <th>Description</th>
                <th>Prescription</th>
            </tr>
            <c:forEach var="medicine" items="${medicines}">
                <tr>
                    <td><c:out value="${medicine.name}"/></td>
                    <td><c:out value="${medicine.price}"/></td>
                    <td><c:out value="${medicine.category}"/></td>
                    <td><c:out value="${medicine.description}"/></td>
                    <td>
                        <c:out value="${medicine.prescription}"/></td>
                    <td>
                        <c:if test="${sessionScope.containsValue(manager)}">
                        <a href='<c:url value="/editMedicine?id=${medicine.id}"/>'>Edit</a>
                        <a href='<c:url value="/deleteMedicine?id=${medicine.id}"/>'>Delete</a>
                        </c:if>
                    <td>
                        <c:if test="${!sessionScope.containsValue(manager)}">
                            <form action="<c:url value='addToCart'/>" method="post">
                                <input type="hidden"
                                       name="id"
                                       value="${medicine.id}"/>
                                <input type="submit"
                                       name="submit"
                                       value="Add to cart">
                            </form>
                        </c:if>
                    </td>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <ul>
            <c:if test="${sessionScope.containsValue(customer)}">
                <form action="/logout" method="post">
                    <input type="submit" value="Logout">
                </form>
            </c:if>
        </ul>
        <c:if test="${!sessionScope.containsValue(manager) && sessionScope.containsValue(cart)}">
            <button onclick="location.href='/showCart'">Show cart</button>
        </c:if>

    </div>
</div>
</body>
</html>
