<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 18.12.2019
  Time: 14:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit customer info</title>
</head>
<body>
<h2>Customer info edit page</h2>
<form method="post">
    <div>
        <label>Name:</label>
        <input type="text" value="${customer.name}" name="name" required><br/>
    </div>
    <div>
        <label>email:</label> <input type="text" value="${customer.email}" name="email" required><br/>
    </div>
    <div>
        <label>address:</label> <input type="text" value="${customer.address}" name="address" required><br/>
    </div>
    <div>
        <label>phone:</label> <input type="text" value="${customer.phone}" name="phone" required><br/>
    </div>
    <div>
        <label>Login:</label><input type="text" value="${customer.login}" name="login" required><br/>
    </div>
    <div>
        <label>Password:</label><input type="password" value="${customer.password}"name="password" required><br/>
    </div>
    <input type="submit" value="Send"/>
</form>
</body>
</html>
