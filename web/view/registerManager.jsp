<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 05.12.2019
  Time: 17:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register manager page</title>
</head>
<body>

<div class="container">
    <h2>Manager registration page</h2>
    <form method="post">
        <div>
            <label>Name:</label>
            <input type="text" name="firstName" required><br/>
        </div>
        <div>
            <label>Last name:</label><input type="text" name="lastName" required><br/>
        </div>
        <div>
            <label>Login:</label> <input type="text" name="login" required><br/>
        </div>
        <div>
            <label>Password:</label><input type="password" name="password" required><br/>
        </div>
        <button type="submit">Submit</button>
    </form>
</div>

</body>
</html>
