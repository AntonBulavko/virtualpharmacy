package ru.vironit.application.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtil {

    private static ConnectionUtil connection;
    private static Connection instance;

    private ConnectionUtil(){
    }

    public static Connection getInstance(){
        if (instance == null){
            try {
                connection = new ConnectionUtil();
                instance = connection.getConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }return instance;
    }

    private Connection getConnection() throws SQLException, IOException {
        Connection connection;
        Properties properties = new Properties();
        InputStream fis = ConnectionUtil.class.getClassLoader().getResourceAsStream("database.properties");
        properties.load(fis);
        String url = properties.getProperty("db.url");
        String username = properties.getProperty("db.username");
        String password = properties.getProperty("db.password");
        String driver = properties.getProperty("db.driver");
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        connection = DriverManager.getConnection(url,username,password);
        return connection;
    }

}