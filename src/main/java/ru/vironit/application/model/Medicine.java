package ru.vironit.application.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "medicines", schema = "pharmacy_schema", catalog = "pharmacy_db")
public class Medicine implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private float price;

    @Column(name = "category")
    private String category;

    @Column(name = "description")
    private String description;

    @Column(name = "prescription")
    private Boolean prescription;

    public Medicine() {
    }

    public Medicine(String name, float price, String category, String description, boolean prescription) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.description = description;
        this.prescription = prescription;
    }

    public Medicine(int id, String name, float price, String category, String description, boolean prescription) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.category = category;
        this.description = description;
        this.prescription = prescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPrescription() {
        return prescription;
    }

    public void setPrescription(boolean prescription) {
        this.prescription = prescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Medicine medicine = (Medicine) o;
        return id == medicine.id &&
                price == medicine.price &&
                prescription == medicine.prescription &&
                name.equals(medicine.name) &&
                category.equals(medicine.category) &&
                description.equals(medicine.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, category, description, prescription);
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", category=" + category +
                ", description='" + description + '\'' +
                ", prescription=" + prescription +
                '}';
    }
}
