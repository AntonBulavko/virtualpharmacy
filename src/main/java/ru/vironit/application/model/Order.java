package ru.vironit.application.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "orders", schema = "pharmacy_schema", catalog = "pharmacy_db")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long orderId;

    @ManyToOne
    @JoinColumn(name = "cust_id", referencedColumnName = "id")
    private Customer customer;

    @Column(name = "total_cost")
    private Double totalCost;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private Collection<OrderItem> items;

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    public Order() {
        items = new ArrayList<>();
    }

    public Order(Customer customer) {
        items = new ArrayList<>();
        this.customer = customer;
    }

    public Order(long orderId, Customer customer) {
        items = new ArrayList<>();
        this.orderId = orderId;
        this.customer = customer;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public Collection<OrderItem> getItems() {
        return items;
    }

    public void setItems(Collection<OrderItem> items) {
        this.items = items;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                items.equals(order.items) &&
                customer.equals(order.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, items, customer);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", items=" + items +
                ",customer=" + customer.getName() +
                ", total cost=" + totalCost +
                '}';
    }
}
