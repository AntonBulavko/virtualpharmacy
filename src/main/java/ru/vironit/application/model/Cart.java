package ru.vironit.application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Cart implements Serializable {

    private ArrayList<OrderItem> items;
    private double totalSum;

    public Cart() {
        items = new ArrayList<>();
    }

    public boolean isEmpty (){
        return items.isEmpty();
    }

    public void clear(){
        items.clear();
    }

    public synchronized void addMedicine(Medicine medicine){
        boolean newItem = true;
        for (OrderItem scItem : items) {
            if (scItem.getMedicine().getId() == medicine.getId()) {
                newItem = false;
                scItem.incrementQuantity();
            }
        }
        if (newItem) {
            OrderItem scItem = new OrderItem(medicine);
            items.add(scItem);
        }
    }

    public synchronized void addMedicine(Medicine medicine, int qty){
        if (qty >= 0) {
            OrderItem item = null;
            for (OrderItem scItem : items) {
                if (scItem.getMedicine().getId() == medicine.getId()) {
                    if (qty != 0) {
                        scItem.setQty(qty);
                    } else {
                        item = scItem;
                        break;
                    }
                }
            }
            if (item != null) {
                items.remove(item);
            }
        }
    }

    public void setItems(ArrayList<OrderItem> items) {
        this.items = items;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public ArrayList<OrderItem> getItems() {
        return items;
    }

    public double getTotalSum() {
        totalSum = 0;
        for (OrderItem item : items){
            totalSum += item.getCost();
        }
        return totalSum;

    }

    @Override
    public String toString() {
        return "Cart{" +
                "items=" + items +
                '}';
    }
}
