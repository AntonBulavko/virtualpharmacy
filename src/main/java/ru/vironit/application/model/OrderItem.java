package ru.vironit.application.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "order_items", schema = "pharmacy_schema", catalog = "pharmacy_db")
public class OrderItem implements Serializable {

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "med_id", referencedColumnName = "id")
    private Medicine medicine;

    @Column(name = "qty")
    private Integer qty;

    public OrderItem() {
    }

    public OrderItem(Medicine medicine) {
        this.medicine = medicine;
        this.qty = 1;
    }

    public OrderItem(Order order, Medicine medicine) {
        this.order = order;
        this.medicine = medicine;
        this.qty = 1;
    }

    public OrderItem(Order order, Medicine medicine, int qty) {
        this.order = order;
        this.medicine = medicine;
        this.qty = qty;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getCost() {
        return medicine.getPrice() * qty;
    }

    public void incrementQuantity() {
        qty++;
    }

    public void decrementQuantity() {
        qty--;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem items = (OrderItem) o;
        return qty == items.qty &&
                medicine.equals(items.medicine);
    }

    @Override
    public int hashCode() {
        return Objects.hash(order, medicine, qty);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "order=" + order.getOrderId() +
                ", medicine=" + medicine +
                ", qty=" + qty +
                '}';
    }
}
