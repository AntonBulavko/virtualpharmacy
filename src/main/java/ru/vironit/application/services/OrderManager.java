package ru.vironit.application.services;

import ru.vironit.application.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderManager {

    private MedicineService medicineService;

    private OrderService orderService;

    private OrderItemService orderItemService;

    public OrderManager() {
        medicineService = new MedicineService();
        orderService = new OrderService();
        orderItemService = new OrderItemService();
    }

    public void placeOrder(Customer customer, Cart cart) {
        Order order = addOrder(customer, cart);
        addOrderItems(order, cart);
    }

    private Order addOrder(Customer customer, Cart cart) {
        Order order = new Order(customer);
        order.setTotalCost(cart.getTotalSum());
        orderService.createOrder(order);
        return order;
    }

    private void addOrderItems(Order order, Cart cart) {
        ArrayList<OrderItem> items = cart.getItems();
        for (OrderItem item : items) {
            Medicine medicine = item.getMedicine();
            OrderItem orderItem = new OrderItem(order, medicine, item.getQty());
            orderItemService.createOrderItems(order, orderItem);
        }
    }

    public Map getOrderDetails(int orderId) {
        Map orderMap = new HashMap();
        Order order = orderService.find(orderId);
        Customer customer = order.getCustomer();
        ArrayList<OrderItem> orderedItems = orderItemService.findByOrderId(orderId);
        List<Medicine> medicines = new ArrayList<>();
        for (OrderItem op : orderedItems) {
            Medicine medicine = medicineService.findOne(op.getMedicine().getId());
            medicines.add(medicine);
        }
        orderMap.put("orderRecord", order);
        orderMap.put("customer", customer);
        orderMap.put("orderedProducts", orderedItems);
        orderMap.put("medicines", medicines);
        return orderMap;
    }
}
