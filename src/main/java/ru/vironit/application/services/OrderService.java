package ru.vironit.application.services;

import ru.vironit.application.dao.JDBC.OrderDAOImpl;
import ru.vironit.application.dao.OrderDAO;
import ru.vironit.application.dao.hibernate.OrderHibernateDAO;
import ru.vironit.application.model.Order;
import java.util.ArrayList;

public class OrderService{

    private OrderDAO orderDAO;

    public OrderService() {
        orderDAO = new OrderHibernateDAO();
    }

    public ArrayList<Order> findAll() {
        return orderDAO.findAll();
    }

    public void createOrder(Order order) {
        orderDAO.createOrder(order);
    }

    public void updateOrder(Order order) {
        orderDAO.updateOrder(order);
    }

    public void deleteOrder(long id) {
        orderDAO.deleteOrder(id);
    }

    public Order find(long orderId) {
        return orderDAO.find(orderId);
    }

    public Order findByCustomer(int customerId) {
        return orderDAO.findByCustomer(customerId);
    }

    public ArrayList<Order> findListByCustomer(int customerId) {
        return orderDAO.findListByCustomer(customerId);
    }
}
