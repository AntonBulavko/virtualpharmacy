package ru.vironit.application.services;

import ru.vironit.application.dao.MedicineDAO;
import ru.vironit.application.dao.hibernate.MedicineHibernateDAO;
import ru.vironit.application.model.Medicine;

import java.util.ArrayList;

public class MedicineService {

    private MedicineDAO medicineDAO;

    public MedicineService() {
        medicineDAO = new MedicineHibernateDAO();
    }

    public void insertMedicine(Medicine medicine) {
        medicineDAO.insertMedicine(medicine);
    }

    public void deleteMedicine(int id) {
        medicineDAO.deleteMedicine(id);
    }

    public void updateMedicine(Medicine medicine) {
        medicineDAO.updateMedicine(medicine);
    }

    public Medicine findOne(int medicineId) {
        return medicineDAO.selectMedicine(medicineId);
    }

    public Medicine findOne(String medicineName) {
        return medicineDAO.selectMedicine(medicineName);
    }

    public ArrayList<Medicine> findAll() {
         return medicineDAO.showAll();
    }
}
