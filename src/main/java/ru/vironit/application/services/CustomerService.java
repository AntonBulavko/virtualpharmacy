package ru.vironit.application.services;

import ru.vironit.application.dao.CustomerDAO;
import ru.vironit.application.dao.hibernate.CustomerHibernateDAO;
import ru.vironit.application.model.Customer;

import java.util.List;

public class CustomerService{

    private CustomerDAO customerDAO;

    public CustomerService() {
        customerDAO = new CustomerHibernateDAO();
    }

    public boolean findByLoginAndPassword(String login, String password) {
        return customerDAO.findByLoginAndPassword(login, password);
    }

    public void create(Customer customer) {
        customerDAO.createCustomer(customer);
    }

    public void update(Customer customer) {
        customerDAO.updateCustomer(customer);
    }

    public void delete(Customer customer) {
        customerDAO.deleteCustomer(customer.getId());
    }

    public Customer selectById(int customerId) {
        return customerDAO.findById(customerId);
    }


    public Customer findByLogin(String login) {
        return customerDAO.findByLogin(login);
    }


    public List<Customer> findAll() {
        return customerDAO.findAll();
    }

}
