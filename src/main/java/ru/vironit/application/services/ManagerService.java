package ru.vironit.application.services;

import ru.vironit.application.dao.ManagerDAO;
import ru.vironit.application.dao.JDBC.ManagerDAOImpl;
import ru.vironit.application.dao.hibernate.ManagerHibernateDAO;
import ru.vironit.application.model.Manager;

import java.util.ArrayList;

public class ManagerService{

    private ManagerDAO managerDAO;

    public ManagerService() {
        managerDAO = new ManagerHibernateDAO();
    }

    public ArrayList selectAll() {
        return managerDAO.selectAll();
    }

    public void create(Manager manager) {
        managerDAO.create(manager);
    }

    public Manager select(String login, String password) {
        return managerDAO.select(login,password);
    }

    public void update(Manager manager) {
        managerDAO.update(manager);
    }

    public void delete(int id) {
         managerDAO.delete(id);
    }
}
