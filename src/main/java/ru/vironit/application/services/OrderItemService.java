package ru.vironit.application.services;

import ru.vironit.application.dao.OrderItemDAO;
import ru.vironit.application.dao.hibernate.OrderItemHibernateDAO;
import ru.vironit.application.model.Order;
import ru.vironit.application.model.OrderItem;

import java.util.ArrayList;

public class OrderItemService {

    private OrderItemDAO orderItemDAO;

    public OrderItemService() {
        orderItemDAO = new OrderItemHibernateDAO();
    }

    public ArrayList<OrderItem> findAll() {
        return orderItemDAO.findAll();
    }

    public void createOrderItems(Order order, OrderItem orderItems) {
        orderItemDAO.createOrderItems(order,orderItems);
    }

    public void updateOrderItems(OrderItem orderItems) {
        orderItemDAO.updateOrderItems(orderItems);
    }

    public void deleteOrderItems(long id) {
        orderItemDAO.deleteOrderItems(id);
    }

    public ArrayList<OrderItem> findByOrderId(long orderId) {
        return orderItemDAO.findByOrderId(orderId);
    }
}
