package ru.vironit.application.trainingRest;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import ru.vironit.application.model.Customer;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "customer", path = "customer")
public interface CustomerRepo extends PagingAndSortingRepository <Customer,Long>{

    List<Customer> findByName(@Param("name") String name);
}
