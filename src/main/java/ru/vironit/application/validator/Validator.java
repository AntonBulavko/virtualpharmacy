package ru.vironit.application.validator;

import javax.servlet.http.HttpServletRequest;

public class Validator {

    public boolean validateQty(String medicineId, String qty) {
        boolean errorFlag = false;
        if (!medicineId.isEmpty() && !qty.isEmpty()) {
            int i = -1;
            try {
                i = Integer.parseInt(qty);
            } catch (NumberFormatException nfe) {
                System.out.println("User did not enter a number in the quantity field");
            }
            if (i < 0 || i > 99) {
                errorFlag = true;
            }
        }
        return errorFlag;
    }


    public boolean validateForm(String name,
                                String email,
                                String address,
                                String phone,
                                String login,
                                String password,
                                HttpServletRequest request) {
        boolean errorFlag = false;
        boolean nameError;
        boolean emailError;
        boolean addressError;
        boolean phoneError;
        boolean loginError;
        boolean passwordError;
        if (name == null
                || name.equals("")
                || name.length() > 45) {
            errorFlag = true;
            nameError = true;
            request.setAttribute("nameError", nameError);
        }
        if (email == null
                || email.equals("")
                || !email.contains("@")) {
            errorFlag = true;
            emailError = true;
            request.setAttribute("emailError", emailError);
        }
        if (phone == null
                || phone.equals("")
                || !phone.contains("+375")
                || phone.length() < 9) {
            errorFlag = true;
            phoneError = true;
            request.setAttribute("phoneError", phoneError);
        }
        if (address == null
                || address.equals("")
                || address.length() > 45) {
            errorFlag = true;
            addressError = true;
            request.setAttribute("addressError", addressError);
        }
        if (login == null
                || login.equals("")
                || login.length() < 4) {
            errorFlag = true;
            loginError = true;
            request.setAttribute("loginError", loginError);
        }
        if (password == null
                || password.equals("")
                || password.length() < 5) {
            errorFlag = true;
            passwordError = true;
            request.setAttribute("passwordError", passwordError);
        }
        return errorFlag;
    }
}
