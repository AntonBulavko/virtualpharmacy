package ru.vironit.application.dao;



import ru.vironit.application.model.Medicine;

import java.util.ArrayList;
import java.util.List;

public interface MedicineDAO {

    void insertMedicine(Medicine medicine);

    Medicine selectMedicine(int id);

    Medicine selectMedicine(String name);

    ArrayList<Medicine> showAll();

    void deleteMedicine(int id);

    void updateMedicine(Medicine medicine);
}
