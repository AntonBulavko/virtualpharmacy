package ru.vironit.application.dao;

import ru.vironit.application.model.Order;
import ru.vironit.application.model.OrderItem;

import java.util.ArrayList;

public interface OrderItemDAO {

    ArrayList<OrderItem> findAll();

    void createOrderItems(Order order,OrderItem orderItems);

    void updateOrderItems(OrderItem orderItems);

    void deleteOrderItems(long id);

    ArrayList<OrderItem> findByOrderId(long orderId);
}
