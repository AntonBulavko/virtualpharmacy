package ru.vironit.application.dao;

import ru.vironit.application.model.Manager;


import java.util.ArrayList;

public interface ManagerDAO {

    ArrayList<Manager> selectAll();

    void create(Manager manager);

    Manager select(String login, String password);

    void update(Manager manager);

    void delete(int id);
}
