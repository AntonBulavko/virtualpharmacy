package ru.vironit.application.dao;

import ru.vironit.application.model.Order;

import java.util.ArrayList;

public interface OrderDAO {

    ArrayList<Order> findAll();

    void createOrder(Order order);

    void updateOrder(Order order);

    void deleteOrder(long id);

    Order find(long orderId);

    Order findByCustomer(int customerId);

    ArrayList<Order> findListByCustomer(int customerId);
}
