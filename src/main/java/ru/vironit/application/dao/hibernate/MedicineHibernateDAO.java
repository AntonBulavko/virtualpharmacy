package ru.vironit.application.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.vironit.application.dao.MedicineDAO;
import ru.vironit.application.model.Medicine;
import ru.vironit.application.utils.HibernateSessionUtil;

import java.util.ArrayList;

public class MedicineHibernateDAO implements MedicineDAO {

    @Override
    public void insertMedicine(Medicine medicine) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(medicine);
        transaction.commit();
        session.close();
    }

    @Override
    public Medicine selectMedicine(int id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Medicine medicine = session.get(Medicine.class, id);
        session.close();
        return medicine;
    }

    @Override
    public Medicine selectMedicine(String name) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Medicine medicine = (Medicine)session.createQuery("from Medicine where name='"+name+"'").uniqueResult();
        session.close();
        return medicine;
    }

    @Override
    public ArrayList<Medicine> showAll() {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<Medicine> medicines = (ArrayList<Medicine>)session.createQuery("from Medicine ").list();
        session.close();
        return medicines;
    }

    @Override
    public void deleteMedicine(int id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(selectMedicine(id));
        transaction.commit();
        session.close();
    }

    @Override
    public void updateMedicine(Medicine medicine) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(medicine);
        transaction.commit();
        session.close();
    }

}
