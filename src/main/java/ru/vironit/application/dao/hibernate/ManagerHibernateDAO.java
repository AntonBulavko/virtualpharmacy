package ru.vironit.application.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.vironit.application.dao.ManagerDAO;
import ru.vironit.application.model.Manager;
import ru.vironit.application.utils.HibernateSessionUtil;

import java.util.ArrayList;

public class ManagerHibernateDAO implements ManagerDAO {

    @Override
    public ArrayList<Manager> selectAll() {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<Manager> managers = (ArrayList<Manager>) session.createQuery("from Manager ").list();
        session.close();
        return managers;
    }

    @Override
    public void create(Manager manager) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(manager);
        transaction.commit();
        session.close();
    }

    @Override
    public Manager select(String login, String password) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Manager manager = (Manager) session.createQuery("from Manager where login='" + login + "'").uniqueResult();
        if (manager.getPassword().equals(password)) {
            return manager;
        } else return null;
    }

    public Manager selectById(int id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Manager manager = session.get(Manager.class, id);
        session.close();
        return manager;
    }

    @Override
    public void update(Manager manager) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(manager);
        transaction.commit();
        session.close();
    }

    @Override
    public void delete(int id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(selectById(id));
        transaction.commit();
        session.close();
    }
}
