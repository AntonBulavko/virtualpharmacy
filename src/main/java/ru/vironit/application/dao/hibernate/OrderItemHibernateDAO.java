package ru.vironit.application.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.vironit.application.dao.OrderItemDAO;
import ru.vironit.application.model.Order;
import ru.vironit.application.model.OrderItem;
import ru.vironit.application.utils.HibernateSessionUtil;
import java.util.ArrayList;

public class OrderItemHibernateDAO implements OrderItemDAO {

    @Override
    public ArrayList<OrderItem> findAll() {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<OrderItem> orderItems = (ArrayList<OrderItem>) session.createQuery("from OrderItem ").list();
        return orderItems;
    }

    @Override
    public void createOrderItems(Order order, OrderItem orderItems) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        orderItems.setOrder(order);
        session.save(orderItems);
        transaction.commit();
        session.close();
    }

    @Override
    public void updateOrderItems(OrderItem orderItems) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(orderItems);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteOrderItems(long id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(findByOrderId(id));
        transaction.commit();
        session.close();
    }

    @Override
    public ArrayList<OrderItem> findByOrderId(long orderId) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<OrderItem> orderItems =(ArrayList<OrderItem>) session.createQuery("from OrderItem where order.id ='"+orderId+"'").list();
        session.close();
        return orderItems;
    }
}
