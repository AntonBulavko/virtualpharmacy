package ru.vironit.application.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.vironit.application.dao.CustomerDAO;
import ru.vironit.application.model.Customer;
import ru.vironit.application.utils.HibernateSessionUtil;

import java.util.List;

public class CustomerHibernateDAO implements CustomerDAO {

    @Override
    public List<Customer> findAll() {
        List<Customer> customers = HibernateSessionUtil.getSessionFactory().openSession().createQuery("from Customer ").list();
        return customers;
    }

    @Override
    public Customer findByLogin(String login) {
        Customer customer = (Customer) HibernateSessionUtil.getSessionFactory().openSession().createQuery("from Customer where login='" + login + "'").uniqueResult();
        return customer;
    }

    @Override
    public boolean findByLoginAndPassword(String login, String password) {
        Customer customer = findByLogin(login);
        if (customer.getPassword().equals(password)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Customer findById(int id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Customer customer = session.get(Customer.class, id);
        session.close();
        return customer;
    }

    @Override
    public void createCustomer(Customer customer) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(customer);
        transaction.commit();
        session.close();
    }

    @Override
    public void updateCustomer(Customer customer) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(customer);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteCustomer(int id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(findById(id));
        transaction.commit();
        session.close();
    }
}
