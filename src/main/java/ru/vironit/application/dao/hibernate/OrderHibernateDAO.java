package ru.vironit.application.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.vironit.application.dao.OrderDAO;
import ru.vironit.application.model.Order;
import ru.vironit.application.utils.HibernateSessionUtil;
import java.util.ArrayList;

public class OrderHibernateDAO implements OrderDAO {

    @Override
    public ArrayList<Order> findAll() {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<Order> orders = (ArrayList<Order>) session.createQuery("from Order ").list();
        session.close();
        return orders;
    }

    @Override
    public void createOrder(Order order) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(order);
        transaction.commit();
        session.close();
    }

    @Override
    public void updateOrder(Order order) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(order);
        transaction.commit();
        session.close();
    }

    @Override
    public void deleteOrder(long id) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(find(id));
        transaction.commit();
        session.close();
    }

    @Override
    public Order find(long orderId) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        Order order = session.get(Order.class, orderId);
        session.close();
        return order;
    }

    @Override
    public Order findByCustomer(int customerId) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<Order> orders = (ArrayList<Order>) session.createQuery("from Order where customer.id='"+customerId+"'").list();
        Order order = orders.get(orders.size()-1);
        session.close();
        return order;
    }

    @Override
    public ArrayList<Order> findListByCustomer(int customerId) {
        Session session = HibernateSessionUtil.getSessionFactory().openSession();
        ArrayList<Order> orders = (ArrayList<Order>) session.createQuery("from Order where customer.id='" + customerId + "'").list();
        return orders;
    }
}
