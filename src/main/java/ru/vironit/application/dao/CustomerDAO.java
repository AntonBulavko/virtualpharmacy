package ru.vironit.application.dao;



import ru.vironit.application.model.Customer;

import java.util.List;

public interface CustomerDAO {

    List<Customer> findAll();

    boolean findByLoginAndPassword(String login, String password);

    Customer findByLogin(String login);

    Customer findById(int id);

    void createCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(int id);

}
