package ru.vironit.application.dao.JDBC;

import ru.vironit.application.utils.ConnectionUtil;
import ru.vironit.application.dao.MedicineDAO;
import ru.vironit.application.model.Medicine;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MedicineDAOImpl implements MedicineDAO {

    private static final String SELECT_ALL = "SELECT * from pharmacy_db.pharmacy_schema.medicines";
    private static final String INSERT = "INSERT into pharmacy_db.pharmacy_schema.medicines( med_name, med_price, med_category, med_description, med_prescription) Values (?,?,?,?,?)";
    private static final String SELECT_ONE = "SELECT * FROM  pharmacy_db.pharmacy_schema.medicines where med_id = ? ";
    private static final String SELECT_ONE_BY_NAME = "SELECT * FROM  pharmacy_db.pharmacy_schema.medicines where med_name = ? ";
    private static final String UPDATE = "UPDATE  pharmacy_db.pharmacy_schema.medicines set med_name = ?, med_price = ?, med_category = ?, med_description = ?, med_prescription = ? where med_id =?";
    private static final String DELETE = "DELETE FROM pharmacy_db.pharmacy_schema.medicines where med_id = ?";

    @Override
    public void insertMedicine(Medicine medicine) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(INSERT)) {
            statement.setString(1, medicine.getName());
            statement.setFloat(2, medicine.getPrice());
            statement.setString(3, String.valueOf(medicine.getCategory()));
            statement.setString(4, medicine.getDescription());
            statement.setBoolean(5, medicine.isPrescription());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Medicine selectMedicine(int id) {
        Medicine medicine = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ONE)) {
            statement.setLong(1, id);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int medicineId = set.getInt("med_id");
                String name = set.getString("med_name");
                float price = set.getFloat("med_price");
                String category = set.getString("med_category");
                String description = set.getString("med_description");
                boolean isPrescription = set.getBoolean("med_prescription");
                medicine = new Medicine(medicineId, name, price, category, description, isPrescription);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return medicine;
    }

    @Override
    public Medicine selectMedicine(String name){
        Medicine medicine = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ONE_BY_NAME)) {
            statement.setString(1, name);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int medicineId = set.getInt("med_id");
                String med_name = set.getString("med_name");
                float price = set.getFloat("med_price");
                String category = set.getString("med_category");
                String description = set.getString("med_description");
                boolean isPrescription = set.getBoolean("med_prescription");
                medicine = new Medicine(medicineId, med_name, price, category, description, isPrescription);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return medicine;
    }

    @Override
    public ArrayList<Medicine> showAll() {
        ArrayList<Medicine> medicines = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ALL)) {
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int id = set.getInt("med_id");
                String name = set.getString("med_name");
                float price = set.getFloat("med_price");
                String category = set.getString("med_category");
                String description = set.getString("med_description");
                boolean isPrescription = set.getBoolean("med_prescription");
                Medicine medicine = new Medicine(id, name, price, category, description, isPrescription);
                medicines.add(medicine);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return medicines;
    }

    @Override
    public void deleteMedicine(int id) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(DELETE)) {
            statement.setLong(1, id);statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateMedicine(Medicine medicine) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(UPDATE)) {
            statement.setString(1, medicine.getName());
            statement.setFloat(2, medicine.getPrice());
            statement.setString(3, medicine.getCategory());
            statement.setString(4, medicine.getDescription());
            statement.setBoolean(5, medicine.isPrescription());
            statement.setLong(6, medicine.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
