package ru.vironit.application.dao.JDBC;

import ru.vironit.application.dao.CustomerDAO;
import ru.vironit.application.model.Customer;
import ru.vironit.application.utils.ConnectionUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerDAOImpl implements CustomerDAO {

    private static final String FIND_ALL ="SELECT * FROM pharmacy_schema.customers";
    private static final String SELECT_BY_LOGIN_AND_PASSWORD = "SELECT * from pharmacy_schema.customers where cust_login = ? and cust_password = ?";
    private static final String SELECT_BY_LOGIN = "SELECT * from pharmacy_schema.customers where cust_login = ?";
    private static final String SELECT_BY_ID = "SELECT * from pharmacy_schema.customers where cust_id = ?";
    private static final String CREATE = "INSERT into pharmacy_db.pharmacy_schema.customers (cust_name, cust_email, cust_address, cust_phone, cust_login, cust_password) VALUES (?,?,?,?,?,?)";
    private static final String UPDATE = "UPDATE pharmacy_schema.customers set cust_name = ?, cust_email = ?, cust_address = ?, cust_phone = ?, cust_login = ?, cust_password = ? where cust_id = ?";
    private static final String DELETE = "DELETE FROM pharmacy_schema.customers where cust_id=?";

    @Override
    public ArrayList<Customer> findAll() {
        ArrayList<Customer> customers = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(FIND_ALL)){
            ResultSet set = statement.executeQuery();
            while (set.next()){
                int custId = set.getInt(1);
                String userName = set.getString(2);
                String email = set.getString(3);
                String address = set.getString(4);
                String phone = set.getString(5);
                String userLogin = set.getString(6);
                String userPassword = set.getString(7);
               Customer customer = new Customer(custId, userName, email, address, phone, userLogin, userPassword);
               customers.add(customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }

    @Override
    public Customer findByLogin(String login){
        Customer customer = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_LOGIN)) {
            statement.setString(1,login);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                int id = set.getInt(1);
                String userName = set.getString(2);
                String email = set.getString(3);
                String address = set.getString(4);
                String phone = set.getString(5);
                String userLogin = set.getString(6);
                String userPassword = set.getString(7);
                customer = new Customer(id, userName, email, address, phone, userLogin, userPassword);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } return customer;
    }

    @Override
    public boolean findByLoginAndPassword(String login, String password) {
        Customer customer = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_LOGIN_AND_PASSWORD)) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int id = set.getInt(1);
                String userName = set.getString(2);
                String email = set.getString(3);
                String address = set.getString(4);
                String phone = set.getString(5);
                String userLogin = set.getString(6);
                String userPassword = set.getString(7);
                customer = new Customer(id, userName, email, address, phone, userLogin, userPassword);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer != null;
    }

    @Override
    public Customer findById(int id) {
        Customer customer = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_ID)) {
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int custId = set.getInt(1);
                String userName = set.getString(2);
                String email = set.getString(3);
                String address = set.getString(4);
                String phone = set.getString(5);
                String userLogin = set.getString(6);
                String userPassword = set.getString(7);
                customer = new Customer(custId, userName, email, address, phone, userLogin, userPassword);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customer;
    }

    @Override
    public void createCustomer(Customer customer) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(CREATE)) {
            statement.setString(1, customer.getName());
            statement.setString(2, customer.getEmail());
            statement.setString(3, customer.getAddress());
            statement.setString(4, customer.getPhone());
            statement.setString(5, customer.getLogin());
            statement.setString(6, customer.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCustomer(Customer customer) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(UPDATE)) {
            statement.setString(1, customer.getName());
            statement.setString(2, customer.getEmail());
            statement.setString(3, customer.getAddress());
            statement.setString(4, customer.getPhone());
            statement.setString(5, customer.getLogin());
            statement.setString(6, customer.getPassword());
            statement.setInt(7, customer.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteCustomer(int id) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(DELETE)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
