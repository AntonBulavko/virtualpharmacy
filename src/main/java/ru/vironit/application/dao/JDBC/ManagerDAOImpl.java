package ru.vironit.application.dao.JDBC;

import ru.vironit.application.dao.ManagerDAO;
import ru.vironit.application.model.Manager;
import ru.vironit.application.utils.ConnectionUtil;
import java.sql.*;
import java.util.ArrayList;

public class ManagerDAOImpl implements ManagerDAO {

    private static final String SELECT_ALL = "SELECT * from pharmacy_db.pharmacy_schema.managers";
    private static final String INSERT = "INSERT into pharmacy_db.pharmacy_schema.managers(first_name, last_name, login, password)  Values (?,?,?,?)";
    private static final String SELECT_ONE = "SELECT * from pharmacy_db.pharmacy_schema.managers where login = ? and password = ?";
    private static final String UPDATE = "UPDATE pharmacy_db.pharmacy_schema.managers SET first_name = ?, last_name = ?, login = ?, password = ? where id = ?";
    private static final String DELETE = "DELETE FROM pharmacy_db.pharmacy_schema.managers where id = ?";

    @Override
    public ArrayList<Manager> selectAll() {
        ArrayList<Manager> managers = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ALL) ) {
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int id = set.getInt(1);
                String firstName = set.getString(2);
                String lastName = set.getString(3);
                String userLogin = set.getString(4);
                String userPassword = set.getString(5);
                Manager manager = new Manager(id, firstName, lastName, userLogin, userPassword);
                managers.add(manager);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return managers;
    }

    @Override
    public void create(Manager manager) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(INSERT)) {
            statement.setString(1, manager.getFirstName());
            statement.setString(2, manager.getLastName());
            statement.setString(3, manager.getLogin());
            statement.setString(4, manager.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Manager select(String login, String password) {
        Manager manager = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ONE)) {
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int id = set.getInt(1);
                String firstName = set.getString(2);
                String lastName = set.getString(3);
                String managerLogin = set.getString(4);
                String managerPassword = set.getString(5);
                manager = new Manager(id, firstName, lastName, managerLogin, managerPassword);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return manager;
    }

    @Override
    public void update(Manager manager) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(UPDATE)) {
            statement.setString(1, manager.getFirstName());
            statement.setString(2, manager.getLastName());
            statement.setString(3, manager.getLogin());
            statement.setString(4, manager.getPassword());
            statement.setInt(5, manager.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(DELETE)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
