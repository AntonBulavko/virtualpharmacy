package ru.vironit.application.dao.JDBC;

import ru.vironit.application.dao.OrderItemDAO;
import ru.vironit.application.model.Medicine;
import ru.vironit.application.model.Order;
import ru.vironit.application.model.OrderItem;
import ru.vironit.application.utils.ConnectionUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrderItemDAOImpl implements OrderItemDAO {

    private static final String SELECT_ALL = "SELECT * FROM pharmacy_schema.order_items";
    private static final String CREATE = "INSERT INTO pharmacy_schema.order_items(order_id, med_id, qty) values (?,?,?)";
    private static final String UPDATE ="UPDATE pharmacy_schema.order_items set med_id=?, qty = ? where order_id = ?";
    private static final String DELETE = "DELETE FROM pharmacy_schema.order_items where order_id = ?";
    private static final String SELECT_BY_ID = "SELECT * FROM pharmacy_schema.order_items where order_id = ?";

    private MedicineDAOImpl medicineDAO = new MedicineDAOImpl();
    private OrderDAOImpl orderDAO = new OrderDAOImpl();

    @Override
    public void createOrderItems(Order order,OrderItem orderItems) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(CREATE)){
            statement.setLong(1,order.getOrderId());
            statement.setInt(2,orderItems.getMedicine().getId());
            statement.setInt(3,orderItems.getQty());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateOrderItems(OrderItem orderItems) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(UPDATE)){
            statement.setInt(1,orderItems.getMedicine().getId());
            statement.setInt(2,orderItems.getQty());
            statement.setLong(3,orderItems.getOrder().getOrderId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOrderItems(long id) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(DELETE)){
            statement.setLong(1,id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<OrderItem> findByOrderId(long orderId) {
        ArrayList<OrderItem> items = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_ID)){
            statement.setLong(1,orderId);
            ResultSet set = statement.executeQuery();
            while (set.next()){
                int ordId = set.getInt(1);
                int medId = set.getInt(2);
                int qty = set.getInt(3);
                OrderItem orderItems = new OrderItem(orderDAO.find(ordId),medicineDAO.selectMedicine(medId), qty);
                items.add(orderItems);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return items;
    }

    @Override
    public ArrayList<OrderItem> findAll() {
        ArrayList<OrderItem> items = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ALL)){
            ResultSet set = statement.executeQuery();
            while (set.next()){
                int orderId = set.getInt(1);
                int medicineId = set.getInt(2);
                Medicine medicine = medicineDAO.selectMedicine(medicineId);
                Order order = orderDAO.find(orderId);
                OrderItem orderItems = new OrderItem(order,medicine);
                items.add(orderItems);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }return items;
    }
}
