package ru.vironit.application.dao.JDBC;


import ru.vironit.application.dao.OrderDAO;
import ru.vironit.application.model.Customer;
import ru.vironit.application.model.Order;
import ru.vironit.application.utils.ConnectionUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrderDAOImpl implements OrderDAO {

    private static final String SELECT_ALL = "SELECT * FROM pharmacy_schema.orders";
    private static final String CREATE = "INSERT into pharmacy_schema.orders (cust_id, total_cost) values (?,?)";
    private static final String UPDATE = "UPDATE pharmacy_schema.orders set cust_id = ? where id = ?";
    private static final String DELETE = "DELETE from pharmacy_schema.orders where id = ?";
    private static final String SELECT_BY_ID = "SELECT * from pharmacy_schema.orders where id = ?";
    private static final String SELECT_BY_CUSTOMER = "SELECT * from pharmacy_schema.orders where cust_id = ?";

    private CustomerDAOImpl customerDAO = new CustomerDAOImpl();

    @Override
    public ArrayList<Order> findAll() {
        ArrayList<Order> orders = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_ALL)) {
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int orderId = set.getInt(1);
                int customerId = set.getInt(2);
                Customer customer = customerDAO.findById(customerId);
                Order order = new Order(orderId, customer);
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public void createOrder(Order order) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(CREATE)) {
            statement.setInt(1, order.getCustomer().getId());
            statement.setDouble(2,order.getTotalCost());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateOrder(Order order) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(UPDATE)) {
            statement.setLong(1, order.getOrderId());
            statement.setInt(2, order.getCustomer().getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOrder(long id) {
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(DELETE)) {
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Order find(long orderId) {
        Order order = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_ID)) {
            statement.setLong(1, orderId);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                int ordId = set.getInt(1);
                int customerId = set.getInt(2);
                Customer customer = customerDAO.findById(customerId);
                order = new Order(ordId, customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public Order findByCustomer(int customerId) {
        Order order = null;
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_CUSTOMER)) {
            statement.setInt(1, customerId);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                long orderId = set.getLong(1);
                int custId = set.getInt(2);
                Customer customer = customerDAO.findById(custId);
                order = new Order(orderId, customer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public ArrayList<Order> findListByCustomer(int customerId) {
        ArrayList<Order> orders = new ArrayList<>();
        try (PreparedStatement statement = ConnectionUtil.getInstance().prepareStatement(SELECT_BY_CUSTOMER)) {
            statement.setInt(1, customerId);
            ResultSet set = statement.executeQuery();
            while (set.next()) {
                long orderId = set.getLong(1);
                int custId = set.getInt(2);
                double total = set.getInt(3);
                Customer customer = customerDAO.findById(custId);
                Order order = new Order(orderId, customer);
                order.setTotalCost(total);
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }
}
