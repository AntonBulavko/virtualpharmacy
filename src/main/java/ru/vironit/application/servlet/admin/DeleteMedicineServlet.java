package ru.vironit.application.servlet.admin;

import ru.vironit.application.services.MedicineService;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/deleteMedicine")
public class DeleteMedicineServlet  extends HttpServlet {

    private MedicineService medicineService;

    @Override
    public void init(){
        medicineService = new MedicineService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        medicineService.deleteMedicine(id);
        HttpSession session = req.getSession();
        session.setAttribute("medicines",medicineService.findAll());
        resp.sendRedirect("/medicines");
    }
}
