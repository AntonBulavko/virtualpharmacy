package ru.vironit.application.servlet.admin;

import ru.vironit.application.model.Medicine;
import ru.vironit.application.services.MedicineService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/editMedicine")
public class EditMedicineServlet extends HttpServlet {

    private MedicineService medicineService;

    @Override
    public void init(){
        medicineService = new MedicineService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.valueOf(req.getParameter("id"));
        Medicine medicine = medicineService.findOne(id);
        System.out.println(medicine);
        req.setAttribute("medicine", medicine);
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/editmedicine.jsp");
        dispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        int id = Integer.valueOf(req.getParameter("id"));
        String name = req.getParameter("name");
        float price = Float.valueOf(req.getParameter("price"));
        String category = req.getParameter("category");
        String description = req.getParameter("description");
        boolean isPrescription = Boolean.valueOf(req.getParameter("prescription"));
        Medicine medicine = new Medicine(id,name,price,category,description,isPrescription);
        medicineService.updateMedicine(medicine);
        session.setAttribute("medicines",medicineService.findAll());
        resp.sendRedirect("/medicines");
    }
}
