package ru.vironit.application.servlet.admin;

import ru.vironit.application.model.Customer;
import ru.vironit.application.model.Manager;
import ru.vironit.application.services.ManagerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/loginAsManager")
public class LoginServlet extends HttpServlet {


    private ManagerService managerService;

    @Override
    public void init() {
        managerService = new ManagerService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/login.jsp");
        dispatcher.forward(req, resp);
    }
    /*String login = request.getParameter("login");
    String password = request.getParameter("password");
    HttpSession session = request.getSession();
    Customer customer = customerService.findByLogin(login);
        if (customer != null){
        if (customerService.findByLoginAndPassword(login,password)){
            session.setAttribute("customer", customer);
            if (session.getAttribute("cart") != null) {
                response.sendRedirect("/showCart");
            } else {
                response.sendRedirect("/mainPage");
            }
        } else {
            session.setAttribute("Error", "Wrong login or password");
            response.sendRedirect("/login");
        }
    } else {
    session.setAttribute("Error", "Wrong login or password");
    response.sendRedirect("/login");}*/

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        Manager manager = managerService.select(login, password);
        if (manager != null) {
            session.setAttribute("manager", manager);
            response.sendRedirect("/adminPage");
        } else {
            session.setAttribute("Error", "Wrong login or password");
            response.sendRedirect("/loginAsManager");
        }
    }

}
