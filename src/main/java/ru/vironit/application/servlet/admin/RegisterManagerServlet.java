package ru.vironit.application.servlet.admin;

import ru.vironit.application.model.Manager;
import ru.vironit.application.services.ManagerService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/registerManager")
public class RegisterManagerServlet extends HttpServlet {

    private ManagerService managerService;

    @Override
    public void init(){
        managerService = new ManagerService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/view/registerManager.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response){
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String username = request.getParameter("login");
        String password = request.getParameter("password");
        Manager manager = new Manager(firstName,lastName,username,password);
        try {
            managerService.create(manager);
            response.sendRedirect("/adminPage");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
