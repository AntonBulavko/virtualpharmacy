package ru.vironit.application.servlet.admin;

import ru.vironit.application.model.Customer;
import ru.vironit.application.services.CustomerService;
import ru.vironit.application.services.OrderService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;


@WebServlet("/customers")
public class ShowCustomersServlet extends HttpServlet {

    private CustomerService customerService;
    private OrderService orderService;

    @Override
    public void init() {
        customerService = new CustomerService();
        orderService = new OrderService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Customer> customers = customerService.findAll();
        HttpSession session = req.getSession();
        session.setAttribute("customers", customers);
        getServletContext().getRequestDispatcher("/view/admin/customers.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
