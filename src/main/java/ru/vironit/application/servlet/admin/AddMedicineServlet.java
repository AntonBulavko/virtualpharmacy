package ru.vironit.application.servlet.admin;

import ru.vironit.application.model.Medicine;
import ru.vironit.application.services.MedicineService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/add")
public class AddMedicineServlet extends HttpServlet {


    private MedicineService medicineService;

    @Override
    public void init() {
        medicineService = new MedicineService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/view/medicine-form.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        String name = request.getParameter("name");
        float price = Integer.parseInt(request.getParameter("price"));
        String category = request.getParameter("category");
        String description = request.getParameter("description");
        boolean isPrescription = Boolean.valueOf(request.getParameter("prescription"));
        Medicine medicine = new Medicine(name, price, category, description, isPrescription);
        if (medicineService.findOne(name) != null) {
            session.setAttribute("Error", "medicine with this name already in list.");
            response.sendRedirect("/add");
        } else {
            medicineService.insertMedicine(medicine);
            session.setAttribute("medicines", medicineService.findAll());
            response.sendRedirect("/medicines");
        }
    }
}
