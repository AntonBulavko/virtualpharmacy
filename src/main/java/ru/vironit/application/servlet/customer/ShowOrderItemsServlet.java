package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.OrderItem;
import ru.vironit.application.services.OrderItemService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/showOrderItems")
public class ShowOrderItemsServlet extends HttpServlet {

    private OrderItemService orderItemService;

    @Override
    public void init(){
        orderItemService = new OrderItemService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));
        ArrayList<OrderItem> items = orderItemService.findByOrderId(id);
        HttpSession session = req.getSession();
        session.setAttribute("orderItems", items);
        req.getRequestDispatcher("/view/showOrderItems.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}