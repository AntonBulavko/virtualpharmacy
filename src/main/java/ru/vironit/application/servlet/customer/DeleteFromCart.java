package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.Cart;
import ru.vironit.application.model.Medicine;
import ru.vironit.application.services.MedicineService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/deleteItem")
public class DeleteFromCart extends HttpServlet {

    private MedicineService medicineService;

    @Override
    public void init() {
        medicineService = new MedicineService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        HttpSession session = req.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        String medicineId = req.getParameter("id");
        Medicine medicine = medicineService.findOne(Integer.parseInt(medicineId));
        cart.addMedicine(medicine, 0);
        if (cart.isEmpty()){
            session.removeAttribute("cart");
            req.getRequestDispatcher("/view/medicines.jsp").forward(req,resp);
        }
        resp.sendRedirect("/showCart");
    }
}
