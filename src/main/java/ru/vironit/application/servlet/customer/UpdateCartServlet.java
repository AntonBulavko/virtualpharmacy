package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.Cart;
import ru.vironit.application.model.Medicine;
import ru.vironit.application.services.MedicineService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/updateCart")
public class UpdateCartServlet extends HttpServlet {

    private MedicineService medicineService;

    @Override
    public void init() {
        medicineService = new MedicineService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        int medicineId = Integer.parseInt(req.getParameter("medicineId"));
        int qty = Integer.parseInt(req.getParameter("qty"));
        if (req.getParameter("qty") == null){
            qty = 0;
        }
        if (qty < 0){
            session.setAttribute("Error", "Quantity must be >= 1");
        }
        Medicine medicine = medicineService.findOne(medicineId);
        cart.addMedicine(medicine, qty);
        session.removeAttribute("Error");
        session.setAttribute("cart", cart);
        resp.sendRedirect("/showCart");

    }

}
