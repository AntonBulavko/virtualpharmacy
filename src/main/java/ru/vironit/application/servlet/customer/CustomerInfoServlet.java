package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.Customer;
import ru.vironit.application.model.Order;
import ru.vironit.application.model.OrderItem;
import ru.vironit.application.services.OrderItemService;
import ru.vironit.application.services.OrderService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/customerInfo")
public class CustomerInfoServlet extends HttpServlet {

    OrderService orderService;
    OrderItemService orderItemService;

    @Override
    public void init() {
        orderItemService = new OrderItemService();
        orderService = new OrderService();

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Customer customer = (Customer) session.getAttribute("customer");
        ArrayList<Order> orders = orderService.findListByCustomer(customer.getId());
        for (Order order : orders){
            ArrayList<OrderItem> items = orderItemService.findByOrderId(order.getOrderId());
            for (OrderItem item : items){
                System.out.println("===============");
                System.out.println(item);
                System.out.println("===============");

            }
    /*        System.out.println("===============");
            System.out.println(order);
            System.out.println("===============");*/
        }
        session.setAttribute("orders", orders);
        getServletContext().getRequestDispatcher("/view/customerinfo.jsp").forward(req, resp);
    }
}
