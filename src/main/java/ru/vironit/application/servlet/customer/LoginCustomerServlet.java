package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.Customer;
import ru.vironit.application.services.CustomerService;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginCustomerServlet extends HttpServlet {

    private CustomerService customerService;

    @Override
    public void init() {
        customerService = new CustomerService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/login.jsp");
        dispatcher.forward(req, resp);
        HttpSession session = req.getSession();
        session.removeAttribute("Error");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        Customer customer = customerService.findByLogin(login);
        if (customer != null){
            if (customerService.findByLoginAndPassword(login,password)){
                session.setAttribute("customer", customer);
                if (session.getAttribute("cart") != null) {
                    response.sendRedirect("/showCart");
                } else {
                    response.sendRedirect("/mainPage");
                }
            } else {
                session.setAttribute("Error", "Wrong login or password");
                response.sendRedirect("/login");
            }
        }
         else {
            session.setAttribute("Error", "Wrong login or password");
            response.sendRedirect("/login");
        }
    }
}
