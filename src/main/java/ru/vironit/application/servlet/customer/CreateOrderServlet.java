package ru.vironit.application.servlet.customer;


import ru.vironit.application.model.Cart;
import ru.vironit.application.model.Customer;
import ru.vironit.application.services.OrderManager;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/createOrder")
public class CreateOrderServlet extends HttpServlet
{
    private OrderManager manager;

    @Override
    public void init() {
        manager = new OrderManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        Customer customer = (Customer) session.getAttribute("customer");
        if (cart != null) {
            if (customer != null) {
                manager.placeOrder(customer, cart);
                session.removeAttribute("cart");
                resp.sendRedirect("/mainPage");
            } else {
                session.setAttribute("Error", "You must login first");
                resp.sendRedirect("/showCart");
            }
        } else {
            session.setAttribute("Error", "You can't create empty order");
            resp.sendRedirect("/showCart");

        }
    }
}

