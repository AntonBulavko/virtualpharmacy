package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.Customer;
import ru.vironit.application.services.CustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/editCustomer")
public class EditCustomerServlet extends HttpServlet {

    private CustomerService customerService;

    @Override
    public void init(){
        customerService = new CustomerService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getParameter("customer");
        RequestDispatcher dispatcher = req.getRequestDispatcher("/view/editcustomer.jsp");
        dispatcher.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        int id = Integer.parseInt(req.getParameter("id"));
        session.removeAttribute("customer");
        String name = req.getParameter("name");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String address = req.getParameter("address");
        String phone = req.getParameter("phone");
        String email = req.getParameter("email");
        Customer customer = new Customer(id,name,email,address,phone,login,password);
        session.setAttribute("customer", customer);
        customerService.update(customer);
        resp.sendRedirect("/customerInfo");
    }
}
