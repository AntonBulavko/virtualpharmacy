package ru.vironit.application.servlet.customer;

import ru.vironit.application.model.Cart;
import ru.vironit.application.model.Medicine;
import ru.vironit.application.services.MedicineService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(loadOnStartup = 1, urlPatterns = {"/medicines", "/addToCart"})
public class MedicineServlet extends HttpServlet {

    private MedicineService medicineService;

    @Override
    public void init() {
        medicineService = new MedicineService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userPath = req.getServletPath();
        HttpSession session = req.getSession();
        if (userPath.equals("/medicines")){
            session.getAttribute("medicines");
        }
        getServletContext().getRequestDispatcher("/view/medicines.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String userPath = req.getServletPath();
        HttpSession session = req.getSession();
        Cart cart = (Cart) session.getAttribute("cart");
        if (userPath.equals("/addToCart")) {
            if (cart == null) {
                cart = new Cart();
                session.setAttribute("cart", cart);
            }
            String medicineId = req.getParameter("id");
            Medicine medicine = medicineService.findOne(Integer.parseInt(medicineId));
            cart.addMedicine(medicine);
        }
        userPath = "/medicines";
        resp.sendRedirect(userPath);

    }
}
